using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSprite : MonoBehaviour
{
    public Sprite[] sprites;


    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.SpriteChange += SpriteChanger;
    }

    public void SpriteChanger()
    {
        int randomIndex = Random.Range(0, sprites.Length);
        GetComponent<SpriteRenderer>().sprite = sprites[randomIndex];
    }
}
