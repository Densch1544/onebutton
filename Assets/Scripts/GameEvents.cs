using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;

    private void Awake()
    {
        current = this;
    }


    public event Action SpriteChange;
    public void ChangeSprite()
    {
        if(SpriteChange != null)
        {
            SpriteChange();                
        }
    }
    public event Action JumpScoreUp;
    public void JumpScore()
    {
        if (JumpScoreUp != null)
        {
            JumpScoreUp();
        }
    }
    public event Action JumpDown;
    public void JumpScoreDown()
    {
        if (JumpDown != null)
        {
            JumpDown();
        }
    }
    public event Action FadeIn;
    public void FadingIn()
    {
        if (FadeIn != null)
        {
            FadeIn();
        }
    }
    public event Action FadeOut;
    public void FadingOut()
    {
        if (FadeOut != null)
        {
            FadeOut();
        }
    }
}
