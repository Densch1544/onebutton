using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class JumpCounter : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textMesh;

    //static to keep value over scene changes
    [SerializeField]
    public static int JumpScore = 11;
    // Start is called before the first frame update
    void Start()

    {
        GameEvents.current.JumpScoreUp += ScoreUp;
        GameEvents.current.JumpDown += ScoreReset;
    }

    public void ScoreReset()
    {
        JumpScore = 11;
    }
    public void ScoreUp()
    {
        JumpScore--;
    }
    protected virtual string GetJumpscoreValue()
    {
        return JumpScore.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        textMesh.text = GetJumpscoreValue();
        if(JumpScore == 0)
        {
            SceneManager.LoadScene("Outro");
            JumpScore = 11;
        }
    }
}
