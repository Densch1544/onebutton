using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float runSpeed;
    [SerializeField] 
    private float m_JumpForce = 400f;
    public bool jump = false;
    public bool canMove = true;

    [SerializeField] 
    private Transform m_GroundCheck;
    [SerializeField] 
    private LayerMask m_WhatIsGround;
    [SerializeField]
    private bool m_Grounded;
    const float k_GroundedRadius = .2f;
    private Rigidbody2D m_Rigidbody2D;

    Vector3 originalPos;
    public AudioSource maeh;
    public AudioSource buzz;
    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

    }

    private void Start()
    {
        originalPos = transform.position;
    }
    void Update()
    {
        this.transform.position = this.transform.position + (new Vector3(runSpeed, 0, 0) * Time.deltaTime);


        if (Input.GetButtonDown("Jump"))
        {
            maeh.Play();
            jump = true;

        }
    }
    void FixedUpdate()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
            }
        }

        if (!canMove)
        {
            return;
        }
        // Move our character
        Jump(runSpeed * Time.fixedDeltaTime, jump);
        jump = false;
    }

    public void Jump(float runSpeed, bool jump)
    {
        // If the player should jump...
        if (m_Grounded && jump)
        {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("End"))
        {
            transform.position = originalPos;
            GameEvents.current.ChangeSprite();
        }
        if (other.CompareTag("Fail"))
        {
            buzz.Play();
            transform.position = originalPos;
            GameEvents.current.ChangeSprite();
            GameEvents.current.FadingOut();
            GameEvents.current.JumpScoreDown();
        }
        if (other.CompareTag("Goal"))
        {
            GameEvents.current.JumpScore();
            GameEvents.current.FadingIn();
        }
    
    }  
}