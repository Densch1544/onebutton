using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepFade : MonoBehaviour
{
    public Color SpriteColor;
    private void Start()
    {
        GameEvents.current.FadeIn += InFading;
        GameEvents.current.FadeOut += OutFading;

        SpriteColor.a = 0f;
    }
    private void Update()
    {
        GetComponent<SpriteRenderer>().color = SpriteColor;        
    }
    public void InFading()
    {
        SpriteColor.a += 0.1f;
    }

    public void OutFading()
    {
        SpriteColor.a = 0f;
    }
}
